package nl.vv32.potterbot2.sources;

import nl.vv32.potterbot2.abc.Song;
import nl.vv32.potterbot2.abc.Source;
import nl.vv32.potterbot2.songs.LocalFileSong;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;

public class TestSource implements Source {

    final private static Path dir = Paths.get("./test/");

    @Override
    public Song getNext() throws IOException {
        final var list = new ArrayList<Path>();
        Files.newDirectoryStream(dir).forEach(list::add);
        Collections.shuffle(list);
        return new LocalFileSong(list.get(0));
    }
}
