package nl.vv32.potterbot2;

import discord4j.core.DiscordClient;
import discord4j.core.DiscordClientBuilder;
import discord4j.core.event.ReactiveEventAdapter;
import discord4j.core.event.domain.VoiceStateUpdateEvent;
import discord4j.core.event.domain.lifecycle.ReadyEvent;
import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.VoiceState;
import discord4j.core.object.entity.Member;
import discord4j.core.object.entity.Message;
import discord4j.voice.VoiceConnection;
import nl.vv32.potterbot2.sources.TestSource;
import org.reactivestreams.Publisher;
import reactor.core.publisher.Mono;
import reactor.core.publisher.SignalType;
import reactor.util.Logger;
import reactor.util.Loggers;

import java.io.IOException;
import java.util.logging.Level;

public class PotterBot {

    final private static Logger LOGGER =
            Loggers.getLogger(PotterBot.class);

    final private DiscordClient client;
    final private PotterBotAudioProvider audioProvider;

    public PotterBot(final String token) throws Exception {

        client = DiscordClientBuilder.create(token).build();
        audioProvider = new PotterBotAudioProvider(new TestSource());

    }

    public Mono<Void> run() {

        return client.withGateway(gateway -> gateway.on(
                new ReactiveEventAdapter() {

            @Override
            public Publisher<?> onReady(ReadyEvent event) {
                return Mono.empty().log("Logged in successfully",
                        Level.INFO,
                        SignalType.ON_SUBSCRIBE);
            }

            @Override
            public Publisher<?> onMessageCreate(
                    final MessageCreateEvent event) {

                final Message message = event.getMessage();

                return switch (
                        message.getContent()
                                .toLowerCase()
                                .split(" ")[0]) {

                    case "/join" -> onJoinCommand(message);
                    case "/next" -> onNextCommand(message);

                    default -> Mono.empty();
                };
            }

            @Override
            public Publisher<?> onVoiceStateUpdate(
                    final VoiceStateUpdateEvent event) {

                return super.onVoiceStateUpdate(event);
            }

            private Publisher<?> onJoinCommand(final Message command) {

                return command.getAuthorAsMember()
                        .flatMap(Member::getVoiceState)
                        .flatMap(VoiceState::getChannel)
                        .flatMap(channel -> channel.join(spec -> spec.setProvider(audioProvider)));
            }

            private Publisher<?> onNextCommand(final Message command) {
                try { //TODO
                    audioProvider.next();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return Mono.empty();
            }
        }));
    }

    private Mono<Void> onVoiceState(final VoiceStateUpdateEvent event) {
        return event.getOld()
                .map(state -> state
                        .getChannel()
                        .filterWhen(channel -> channel
                                .getVoiceStates()
                                .count()
                                .map(count -> count == 1))
                        .then(state
                                .getChannel()
                                .flatMap(channel -> channel
                                        .getVoiceConnection()
                                        .flatMap(VoiceConnection::disconnect))))
                .orElseGet(Mono::empty);
    }

    public static void main(final String[] args) throws Exception {
        new PotterBot(args[0]).run().block();
    }
}
