package nl.vv32.potterbot2;

import discord4j.voice.AudioProvider;
import nl.vv32.potterbot2.abc.Song;
import nl.vv32.potterbot2.abc.Source;
import nl.vv32.potterbot2.util.OpusAudioReader;
import reactor.util.Logger;
import reactor.util.Loggers;

import java.io.IOException;

public class PotterBotAudioProvider extends AudioProvider {

    final private Logger LOGGER =
            Loggers.getLogger(PotterBotAudioProvider.class);

    final private static int EXCEPTION_TIMEOUT = 5000; // 5 seconds

    final private Source songSource;

    public PotterBotAudioProvider(final Source songSource) {
        this.songSource = songSource;
    }

    private OpusAudioReader inputStream;

    public synchronized void next() throws IOException {
        inputStream = loadNext();
    }

    private long lastError = Long.MIN_VALUE;

    @Override
    public synchronized boolean provide() {
        // Don't provide audio if an exception happened recently
        if (System.currentTimeMillis() - lastError < EXCEPTION_TIMEOUT) {
            //return false;
        }
        try {
            getBuffer().clear();

            while (inputStream == null || !inputStream.read(getBuffer())) {
                // End of stream
                inputStream = loadNext();
            }
            getBuffer().flip();

            return true;

        } catch (final IOException exception) {
            LOGGER.warn("Unable to provide audio", exception);

            try {
                if (inputStream != null) {

                    inputStream.close();
                    inputStream = null;
                }
            } catch (final IOException closeException) {
                LOGGER.warn("Unable to close input");
            }
            lastError = System.currentTimeMillis();

            return false;
        }
    }

    private OpusAudioReader loadNext() throws IOException {
        if (inputStream != null) {
            inputStream.close();
        }

        final Song song = songSource.getNext();

        System.out.println(song.getIdentifier());

        return OpusAudioReader.load(song.read());
    }
}
