package nl.vv32.potterbot2.util;

import org.chenliang.oggus.opus.AudioDataPacket;
import org.chenliang.oggus.opus.OggOpusStream;
import org.chenliang.oggus.opus.OpusPacket;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.LinkedList;
import java.util.Queue;

public class OpusAudioReader implements Closeable {

    final private InputStream inputStream;
    final private OggOpusStream oggStream;

    private OpusAudioReader(final InputStream inputStream,
                            final OggOpusStream oggStream) {
        this.inputStream = inputStream;
        this.oggStream = oggStream;
    }

    public static OpusAudioReader load(final InputStream inputStream)
            throws IOException {

        return new OpusAudioReader(inputStream,
                OggOpusStream.from(inputStream));
    }

    private Queue<OpusPacket> packetQueue;

    public boolean read(final ByteBuffer buffer) throws IOException {

        while (packetQueue == null || packetQueue.isEmpty()) {
            final AudioDataPacket audioDataPacket = oggStream.readAudioPacket();

            if (audioDataPacket != null) {
                packetQueue =
                        new LinkedList<>(audioDataPacket.getOpusPackets());
            } else {
                // End of stream
                return false;
            }
        }
        buffer.put(packetQueue.remove().dumpToStandardFormat());

        return true;
    }

    @Override
    public void close() throws IOException {
        inputStream.close();
    }
}
