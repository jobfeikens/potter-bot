package nl.vv32.potterbot2.abc;

import java.io.IOException;
import java.io.InputStream;

public interface Song {

    String getIdentifier() throws IOException;

    String getMimeType() throws IOException;

    InputStream read() throws IOException;
}
