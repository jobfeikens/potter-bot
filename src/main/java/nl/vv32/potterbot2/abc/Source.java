package nl.vv32.potterbot2.abc;

import java.io.IOException;

public interface Source {

    Song getNext() throws IOException;
}
