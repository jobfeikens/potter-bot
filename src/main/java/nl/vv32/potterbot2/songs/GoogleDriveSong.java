package nl.vv32.potterbot2.songs;

import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.File;
import nl.vv32.potterbot2.abc.Song;

import java.io.IOException;
import java.io.InputStream;

public class GoogleDriveSong implements Song {

    final private Drive service;
    final private File file;

    public GoogleDriveSong(final Drive service, final File file) {
        this.service = service;
        this.file = file;
    }

    @Override
    public String getIdentifier() {
        return file.getName();
    }

    @Override
    public String getMimeType() {
        return file.getMimeType();
    }

    @Override
    public InputStream read() throws IOException {

        return service.files()
                .get(file.getId())
                .executeMediaAsInputStream();
    }
}
