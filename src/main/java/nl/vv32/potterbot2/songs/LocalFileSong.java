package nl.vv32.potterbot2.songs;

import nl.vv32.potterbot2.abc.Song;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;

public record LocalFileSong(Path path) implements Song {

    @Override
    public String getIdentifier() throws IOException {
        return path.getFileName().toString();
    }

    @Override
    public String getMimeType() throws IOException {
        return Files.probeContentType(path);
    }

    @Override
    public InputStream read() throws IOException {
        return new FileInputStream(path.toFile());
    }
}
