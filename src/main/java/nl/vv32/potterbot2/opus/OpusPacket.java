package nl.vv32.potterbot2.opus;

import java.io.IOException;
import java.nio.ByteBuffer;

public abstract class OpusPacket {

    // Table-of-contents (TOC) header
    public abstract byte readHeaderByte() throws IOException;

    public abstract void readContent(ByteBuffer dst) throws IOException;

    public OpusPacketHeader readHeader() throws IOException {
        return new OpusPacketHeader(readHeaderByte());
    }

    public void readStandard(ByteBuffer dst) throws IOException {
        dst.put(readHeaderByte());
        readContent(dst);
    }
}
