package nl.vv32.potterbot2.opus;


// Section 2 of https://datatracker.ietf.org/doc/html/rfc6716
public enum OpusBandwidth {

    NARROW_BAND(4000, 8000),
    MEDIUM_BAND(6000, 12000),
    WIDEBAND(8000, 16000),
    SUPER_WIDEBAND(12000, 24000),

    // Although the sampling theorem allows a bandwidth as large as half
    //   the sampling rate, Opus never codes audio above 20 kHz, as that is
    //   the generally accepted upper limit of human hearing.
    FULLBAND(20000, 48000);

    final public int audioBandwidth;

    // Effective sample rate
    final public int sampleRate;

    OpusBandwidth(final int audioBandwidth, final int sampleRate) {
        this.audioBandwidth = audioBandwidth;
        this.sampleRate = sampleRate;
    }
}
