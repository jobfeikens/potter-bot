package nl.vv32.potterbot2.opus;

import java.time.Duration;

public class OpusPacketHeader {

    final public byte headerByte;

    public OpusPacketHeader(final byte headerByte) {
        this.headerByte = headerByte;
    }

    public PacketConfiguration getPacketConfiguration() {
        return new PacketConfiguration(headerByte & 0b11111000 >> 3);
    }

    public OperatingMode getOperatingMode() {
        return getPacketConfiguration().getOperatingMode();
    }

    public OpusBandwidth getBandwidth() {
        return getPacketConfiguration().getBandwidth();
    }

    public int getAudioBandwidth() {
        return getBandwidth().audioBandwidth;
    }

    public int getSampleRate() {
        return getBandwidth().sampleRate;
    }

    public Duration getFrameSize() {
        return getPacketConfiguration().getFrameSize();
    }

    public boolean isStereo() {
        return (headerByte & 0b00000100) > 0;
    }
}
