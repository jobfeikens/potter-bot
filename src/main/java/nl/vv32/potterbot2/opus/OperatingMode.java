package nl.vv32.potterbot2.opus;

// Section 3.1 of https://datatracker.ietf.org/doc/html/rfc6716
public enum OperatingMode {

    SILK_ONLY,
    CELT_ONLY,
    HYBRID,
}
