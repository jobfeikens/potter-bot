package nl.vv32.potterbot2.opus.rfc3533.util;

import nl.vv32.potterbot2.opus.rfc3533.OggPacket;

public interface PacketConsumer {

    PacketAction accept(OggPacket packet);
}
