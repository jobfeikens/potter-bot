package nl.vv32.potterbot2.opus.rfc3533;

import java.io.IOException;
import java.io.InputStream;
import java.io.SequenceInputStream;
import java.util.Enumeration;
import java.util.Iterator;

public class OggPacketReader {

    OggPage page;

    public OggPacket readNextPacket() throws IOException {

        var input = new SequenceInputStream(new Enumeration<>() {
            int pageIndex = 0;

            @Override
            public boolean hasMoreElements() {
                return pageIndex == 0 || page.continuesLastPage();
            }

            @Override
            public InputStream nextElement() {
                page = getNextPage();
                pageIndex++;

                return new SequenceInputStream(new Enumeration<>() {
                    int sequenceIndex = 0;

                    @Override
                    public boolean hasMoreElements() {
                        return sequenceIndex < page.getNumberPageSegments();
                    }

                    @Override
                    public InputStream nextElement() {
                        return null;
                    }
                });
            }
        });
        return new OggPacket(input);
    }

    private OggPage getNextPage() {

    }

    private InputStream readPageSegments(Page page) {

    }
}
