package nl.vv32.potterbot2.opus.rfc3533;


import java.io.DataInputStream;
import java.io.IOException;

/**
 *    The Ogg page header has the following format:
 *
 *  0                   1                   2                   3
 *  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1| Byte
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * | capture_pattern: Magic number for page start "OggS"           | 0-3
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * | version       | header_type   | granule_position              | 4-7
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |                                                               | 8-11
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |                               | bitstream_serial_number       | 12-15
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |                               | page_sequence_number          | 16-19
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |                               | CRC_checksum                  | 20-23
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |                               |page_segments  | segment_table | 24-27
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * | ...                                                           | 28-
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 */

public record OggPageHeader(byte[] segmentTable) {

    public static OggPageHeader read(final DataInputStream inputStream)
            throws IOException {

        inputStream.skipNBytes(26);

        //7e
        byte[] segmentTable =
                new byte[Byte.toUnsignedInt(inputStream.readByte())];

        // [13]
        inputStream.readFully(segmentTable);

        return new OggPageHeader(segmentTable);
    }

    public int getPageSize() {
        int pageSize = 0;

        for (byte lacingValue : segmentTable) {
            pageSize += Byte.toUnsignedInt(lacingValue);
        }
        return pageSize;
    }
}
