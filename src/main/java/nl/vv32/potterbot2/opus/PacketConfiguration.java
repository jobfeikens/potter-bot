package nl.vv32.potterbot2.opus;

import java.time.Duration;

public class PacketConfiguration {

    final public int number;

    public PacketConfiguration(final int configurationNumber) {
        this.number = configurationNumber;
    }

    public OperatingMode getOperatingMode() {
        if (number < 12) {
            return OperatingMode.SILK_ONLY;
        } else if (number < 16) {
            return OperatingMode.HYBRID;
        } else {
            return OperatingMode.CELT_ONLY;
        }
    }

    public OpusBandwidth getBandwidth() {
        if (number < 4) {
            return OpusBandwidth.NARROW_BAND;
        } else if (number < 8) {
            return OpusBandwidth.MEDIUM_BAND;
        } else if (number < 12) {
            return OpusBandwidth.WIDEBAND;
        } else if (number < 14) {
            return OpusBandwidth.SUPER_WIDEBAND;
        } else if (number < 16) {
            return OpusBandwidth.FULLBAND;
        } else if (number < 20) {
            return OpusBandwidth.NARROW_BAND;
        } else if (number < 24) {
            return OpusBandwidth.WIDEBAND;
        } else if (number < 28) {
            return OpusBandwidth.SUPER_WIDEBAND;
        } else {
            return OpusBandwidth.FULLBAND;
        }
    }

    public int getAudioBandwidth() {
        return getBandwidth().audioBandwidth;
    }

    public int getSampleRate() {
        return getBandwidth().sampleRate;
    }

    public Duration getFrameSize() {
        final int micros;

        switch (number) {
            case 16:
            case 24:
            case 20:
            case 28:
                micros = 2500;
                break;
            case 17:
            case 25:
            case 21:
            case 29:
                micros = 5000;
                break;
            case 0:
            case 4:
            case 8:
            case 22:
            case 18:
            case 12:
            case 14:
            case 26:
            case 30:
                micros = 10000;
                break;
            case 1:
            case 5:
            case 23:
            case 19:
            case 9:
            case 13:
            case 15:
            case 27:
            case 31:
                micros = 20000;
                break;
            case 2:
            case 6:
            case 10:
                micros = 40000;
                break;
            case 3:
            case 7:
            case 11:
                micros = 60000;
                break;
            default:
                throw new RuntimeException("Cannot reach");
        }
        return Duration.ofNanos(micros * 1000);
    }
}
