package nl.vv32.potterbot2.opus;

import java.time.Duration;

public class OpusFrame {

    public int bitrate;
    public int channels;
    public int bandwidth;
    public Duration duration;
}
